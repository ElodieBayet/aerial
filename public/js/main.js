'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/** @file Main Program for Modules – 'defer' loaded src */

/**
 * Cross-Page requirements and management
 */
import GUIServices from './modules/guiServices.js';
import Opener from './modules/opener.js';

(() => {
	const _NAV = {
		container: document.querySelector('#uihead'),
		triggers: document.querySelectorAll('#uihead .uinav'),
		receptor: document.querySelector('#uihead .navigation')
	}

	const _SHARE = {
		container: document.querySelector('#share'),
		triggers: document.querySelectorAll('#share button'),
		receptor: document.querySelector('#share ul')
	}
	
	const menu = GUIServices.implement( _NAV, Opener ) || GUIServices.moduleError('Menu');

	const share = GUIServices.implement( _SHARE, Opener ) || GUIServices.moduleError('Partage');
	
	if ( menu instanceof Opener ){
		GUIServices.delayedResizer( menu.autoCompute ); // TODO? pass innerWidth limit of window
	}
})();


/** 
 * Module requirement detections
 */
if (document.querySelectorAll('a.anchor').length && getComputedStyle( document.querySelector('#uihead') ).getPropertyValue('position') === 'sticky') {
	// Adjust scrolling because of sticky header
	import('./modules/scroller.js')
		.then( ({default: Scroller}) => { 
			const anchors = document.querySelectorAll('a.anchor');
			const header = document.querySelector('#uihead'); // [!] #uihead query... again
			const scroller = new Scroller(anchors, header);
		})
		.catch( Errors => {
			GUIServices.moduleError('Scroller');
			console.error(Errors);
		});
}