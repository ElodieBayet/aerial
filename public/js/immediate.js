'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/** @file Initial Program for GUI – 'async' loaded src */

/** Remove class '.nojs' : Allow enhanced GUI */
document.documentElement.removeAttribute('class');

/** Current language detection : Global access */
const VL = (() => document.documentElement.lang || null)();