/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Auto-builder of DOM nodes
 * * Build nodes side-by-side
 * * Build empty tags
 * * Build nodes inside others
 */
class DOMBuilder {
    
    constructor(){
        this._tagList = ['a','abbr','acronym','address','applet','area','article','aside','audio','b','base','basefont','bdi','bdo','big','blockquote','body',
        'br','button','canvas','caption','center','cite','code','col','colgroup','data','datalist','dd','del','details','dfn','dialog','dir','div','dl','dt',
        'em','embed','fieldset','figcaption','figure','font','footer','form','frame','frameset','h1','h2','h3','h4','h5','h6','head','header','hr','html','i',
        'iframe','img','input','ins','kbd','label','legend','li','link','main','map','mark','meta','meter','nav','noframes','noscript','object','ol','optgroup',
        'option','output','p','param','picture','pre','progress','q','rp','rt','ruby','s','samp','script','section','select','small','source','span','strike',
        'strong','style','sub','summary','sup','svg','table','tbody','td','template','textarea','tfoot','th','thead','time','title','tr','track','tt','u','ul',
        'var','video','wbr']; // cf. W3School : HTML Element Reference on 2021.03
    }

    get tagList(){
        return this._tagList;
    }

    /**
     * Insert content into node
     * @param {(String|Object)} inner - If string, it creates textNode
     * @returns {Object} - Inner Element created
     */
    _contentCreator(inner) {
        if(typeof inner === 'string') inner = document.createTextNode(inner);
        return inner;
    }

    /**
     * Create instance of DOM Element
     * @param {String} node - Node name to build, i.e. 'p'
     * @param {(String|Array|Null)} content - Content to insert into the node. A String for textContent ; An array of other DOM elements or Strings ; or null if nothing.
     * @param {(Object|Null)} attributes - Attributes to add. Object of keys as DOM property names, i.e. 'className:"my-class"'
     * @returns {Object} - DOM element
     */
    nodeCreator(node, content = null, attributes = null){
        if (this._tagList.indexOf(node) > 0) {
            // Build HTML Element Node
            node = document.createElement(node);
            // Add attributes
            if (attributes) {
                for (let key in attributes) {
                    // [!] - 'key' must be attribute name as DOM property. I.e. for 'class' use 'className'
                    node[key] = attributes[key];
                }
            }
            // Add HTML Element Content
            if (content) {
                if (Array.isArray(content)) {
                    content.forEach( insert => { 
                        node.appendChild( this._contentCreator(insert) );
                    } )
                } else {
                    node.appendChild( this._contentCreator(content) );
                }
            }
            return node;
        } else {
            console.error( `Can't insert a non-existing node.`);
            return;
        }
    }
}

export default new DOMBuilder;