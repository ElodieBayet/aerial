'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

import AnecdotesLoaderModel from './anecdotes-loader/anecdotesLoaderModel.js';
import AnecdotesLoaderController from './anecdotes-loader/anecdotesLoaderController.js';
import AnecdotesLoaderView from './anecdotes-loader/anecdotesLoaderView.js';

(() => {
    const button = document.querySelector('#anecdote button');
    const para = document.querySelector('#anecdote p');

    const model = new AnecdotesLoaderModel();
    const view = new AnecdotesLoaderView(button, para);
    
    const anecdotesLoader = new AnecdotesLoaderController(model, view);
})();