'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

import SkillsFilterView from './skills-filter/skillsFilterView.js';

(() => {
    const togglers = document.querySelectorAll('.filters a.toggle');
    const reseter = document.querySelector('.filters a.reset');
    const targets = document.querySelectorAll('.skill');

    const skillsFilter = new SkillsFilterView(togglers, reseter, targets);
})();