'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

import FormValidatorController from './form-validator/formValidatorController.js';
import FormValidatorModel from './form-validator/formValidatorModel.js';
import FormValidatorView from './form-validator/formValidatorView.js';


(() => {
	const form = document.querySelector('#hello');

    const model = new FormValidatorModel();
    const view = new FormValidatorView(form);
    
	const validator = new FormValidatorController(model, view);
})();