/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Manage Data model of Form
 * @requires VL constant for current language in /js/immediate.js
 */
class FormValidatorModel {

    constructor(){
        this._vl = VL || 'fr';
        this._errors = { 
            'fr': [
                "Ce champ doit être duement complété",
                "La valeur du champ est incorrecte",
                "L'une de ces options doit être cochée",
                "Cette option doit être acceptée",
                "Ce formulaire a été corrompu, veuillez recharger la page."
            ], 
            'en' : [
                "This field must be filled and suitable",
                "The value of this field is invalid",
                "One of these boxes must be checked",
                "This option must be accepted",
                "This form has been corrupted, please relaod the page."
            ]
        };
    }
    
    initData(form){
        this._formdata = new FormData(form);
        
        for (let key of this._formdata.keys()) {
            if (key === 'info' || key.match(/^soluce/)) this._formdata.delete(key);
        }
        
        if (!this._formdata.get('courtesy')) this._formdata.append('courtesy', '');
        
        if (!this._formdata.get('policy')) this._formdata.append('policy', '');
        
        // Preview & Precheck
        for (let key of this._formdata.keys()) {
            console.info(key, ':', this._formdata.get(key));
            this._setData(key, this._formdata.get(key));
        }
    }

    _courtesyTest(value){
        return (value.match(/(mrs|madame)/i) || value.match(/(mr|monsieur)/i));
    }
    _firstnameTest(value){
        return (value.length >= 2 && value.length <= 100);
    }
    _lastnameTest(value){
        return this._firstnameTest(value);
    }
    _addressTest(value){
        return /^[a-z0-9.\-_+]+@[a-z0-9.-]+\.[a-z]{2,14}$/.test(value);
    }
    _subjectTest(value){
        return (value.length >= 5 && value.length <= 100)
    }
    _messageTest(value){
        return (value.length >= 2 && value.length <= 500)
    }
    _policyTest(value){
        return (value === 'agree');
    }
    _setData(key, value){
        let state = this[`_${key}Test`](value);
        value = state ? value : '';
        this._formdata.set(key, value);
        return state;
    }

    isValid(){
        let hasError = true;
        for ( let entry of this._formdata.entries() ) {
            if( entry[1] === '' ) hasError = false;
        }
        return hasError;
    }

    fieldSet(name, value){
        if( this[`_${name}Test`] ){
            let state = this._setData(name, value);
            this.fieldEdited(name, state, this._errors[this._vl][0]);
        } else {
            this.formCorrupted(this._errors[this._vl][4]);
        }
    }
    
    bindFieldEdited(callback){
        this.fieldEdited = callback
    }

    bindFormCorrupted(callback){
        this.formCorrupted = callback;
    }
}

export default FormValidatorModel;