/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */


/**
 * Handles and Binds Model and View for Form Validator
 * @param {FormValidatorModel} model Manage Dataset of From
 * @param {FormValidatorView} view Manage view of Form
 */
class FormValidatorController {

    constructor(model, view){
        this._model = model;
        this._view = view;

        this._view.bindEditField( this.handleControl );
        this._model.bindFieldEdited( this.onFieldEdit );
        this._model.bindFormCorrupted( this.onFormCorrupt );
        this._view.bindSubmitForm( this.handleSubmit );
        this._view.bindLoadForm( this.handleFormLoad );
    }
    
    handleFormLoad = form => {
        this._model.initData(form);
    }

    handleControl = (name, value) => {
        this._model.fieldSet(name, value);
    }

    handleSubmit = evt => {
        if( this._model.isValid() ) this._view.waitSubmit();
        else evt.preventDefault();
    }

    onFieldEdit = (name, status, msg) => {
        this._view.fieldStatus(name, status, msg);
        this._view.submitStatus( this._model.isValid() );
    }

    onFormCorrupt = msg => {
        this._view.formStatus(msg);
    }
}

export default FormValidatorController;