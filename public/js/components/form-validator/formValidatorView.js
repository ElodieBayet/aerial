/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Manage view of From Validator
 * @param {Object} trigger HTML element that reach next anecdote
 * @param {Object} target HTML element where displaying anecdote
 */
class FormValidatorView {

    constructor(form){
        this._form = form;
        
        // Local detection of elements
        this._submit = form.querySelector('input[type="submit"]');
        this._radios = form.querySelectorAll('input[type="radio"]');
        this._textareas = form.querySelectorAll('textarea');
        this._inputs = form.querySelectorAll('input[type="text"], input[type="email"]');
        
        // DOM for sending status
        this._spiner = document.createElement('div');
        this._spiner.setAttribute('class', 'spin');
    }

    _sanitizeField(value){
        value = value.trim();
        return value.replace(/<{1}[a-z]+\s*[a-z0-9\s\-_"'=\.\/]*>{1}/gi, '').replace(/<{1}\/{1}[a-z]+>/gi, '');
    }

    waitSubmit(){
        // Freeze all field
        [...this._radios, ...this._textareas, ...this._inputs, this._submit].forEach( item => {
            item.setAttribute('readonly', true);
        })
        // Blur form
        this._form.classList.add('onload');
        // Insert spiner
        this._form.appendChild(this._spiner);
    }

    fieldStatus(name, status, msg){
        let bloc = this._form.querySelector(`[name="${name}"]`);
        bloc = name === 'courtesy' || name === 'policy' ? bloc.parentNode.parentNode : bloc.parentNode;
        if (status) {
            bloc.classList.remove('invalid');
        } else {
            bloc.classList.add('invalid');
        }
        bloc.querySelector('p').innerHTML = status ? '' : msg;
    }

    formStatus(msg){
        // TO DO :: insert tag in DOM with error message/info
        console.error(msg);
    }

    submitStatus(status){
        this._submit.disabled = !status;
    }

    bindLoadForm(handler){
        handler(this._form);
    }

    bindEditField(handler){
        this._radios.forEach( radio => {
            radio.addEventListener('click', evt => {
                evt.target.value = this._sanitizeField(evt.target.value);
                handler(evt.target.name, evt.target.value);
            });
        });
        this._textareas.forEach( textarea => {
            textarea.addEventListener('focusout', evt => {
                evt.target.value = this._sanitizeField(evt.target.value);
                handler(evt.target.name, evt.target.value);
            });
        });
        this._inputs.forEach( input => {
            input.addEventListener('focusout', evt => {
                evt.target.value = this._sanitizeField(evt.target.value);
                handler(evt.target.name, evt.target.value);
            });
        });
    }

    bindSubmitForm(handler){
        this._form.addEventListener('submit', evt => {
            handler(evt);
        })
    }
}

export default FormValidatorView;