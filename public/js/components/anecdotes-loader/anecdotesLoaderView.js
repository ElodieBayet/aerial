/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Manage view of Anecdote Component
 * @param {Object} trigger HTML element that reach next anecdote
 * @param {Object} target HTML element where displaying anecdote
 */
class AnecdoteLoaderView{
    constructor(trigger, target){
        this._trigger = trigger;
        this._target = target;
        // Additional DOM element for "freez" effect...
        this._spiner = document.createElement('div');
        this._spiner.setAttribute('class', 'spin');
    }

    /**
     * Replace target content with anecdote
     * @param {String} anecdote Anecdote to display
     */
    displayAnecdote(anecdote){
        this._target.innerHTML = anecdote;
    }

    /**
     * Reglace target with error message and disable trigger
     * @param {String} error Error message to display
     */
    displayError(error){
        this._target.innerHTML = error;
        this._trigger.classList.add('disabled');
    }
    
    /**
     * Very useless but pretty effect if-and-only-if loading is slow
     * @param {Boolean} status True for Freezing and false for unfreezing
     */
    freeze(status){
        if (status) {
            this._target.parentNode.classList.add('onload');
            this._trigger.classList.add('disabled');
            this._target.parentNode.appendChild(this._spiner);
        } else {
            this._target.parentNode.classList.remove('onload');
            this._trigger.classList.remove('disabled');
            this._spiner.remove();
        }
    }

    /**
     * Binding trigger action and anecdote reacher
     * @param {Function} handler To call next anecdote
     */
    bindNextAnecdote(handler){
        this._trigger.addEventListener('click', handler);
    }
}

export default AnecdoteLoaderView;