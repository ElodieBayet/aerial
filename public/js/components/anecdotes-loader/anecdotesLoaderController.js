/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Handles and Binds Model and View for Anecdotes Component
 * @param {AnecdotesLoaderModel} model Manage Dataset of Anecdotes
 * @param {AnecdotesLoaderView} view Manage view of Anecdotes
 */
class AnecdotesLoaderController {

    constructor(model, view){
        this._model = model;
        this._view = view;

        this._view.bindNextAnecdote( this.handleNextAnecdote );
        this._model.bindDataReached( this.onDataReach );
        this._model.bindDataUnreached( this.onDataUnreach );
        
        this.init();
    }

    /** Handle useless effect of frozen-box during loading */
    async init() {
        this._view.freeze(true);
        await this._model.loadData();
        this._view.freeze(false);
    }

    onDataReach = anecdote => {
        this._view.displayAnecdote(anecdote);
    }

    onDataUnreach = error => {
        this._view.displayError(error);
    }

    handleNextAnecdote = () => {
        this._model.getNextAnecdote();
    }
}

export default AnecdotesLoaderController;