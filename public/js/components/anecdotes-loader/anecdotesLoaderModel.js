/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

/**
 * Manage Dataset of Anecdotes Component
 * @requires VL constant for current language in /js/immediate.js
 */
class AnecdotesLoaderModel {

    constructor(){
        this._data = [];
        this._vl = VL || 'fr';
        this._error = {
            fr : "Aucune anecdote trouvée.",
            en : "No anecdote found."
        }
        this._ix = 0;
    }

    /**
     * Load JSon set and store
     */
    loadData() {
        fetch(`/data/common/anecdotes-${this._vl}.json`)
            .then( data => data.json())
            .then( data => {
                this._data = data;
                this.dataReached(this._data[this._ix]);
            }).catch(error => {
                console.error(error);
                this.dataUnreached(this._error[this._vl]);
            });
    }

    /**
     * Compute next anecdote selection and send it
     */
    getNextAnecdote(){
        this._ix += (this._ix+1 < this._data.length)? 1 : (-this._ix);
        this.dataReached(this._data[this._ix]);
    }

    /**
     * View Binding if data reached
     * @param {Function} callback To display anecdote
     */
    bindDataReached(callback){
        this.dataReached = callback;
    }

    /**
     * View Binding if data unreached
     * @param {Function} callback To display error
     */
    bindDataUnreached(callback){
        this.dataUnreached = callback;
    }
}

export default AnecdotesLoaderModel;