'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

import GalleryViewerController from './gallery-viewer/galleryViewerController.js';
import GalleryViewerView from './gallery-viewer/galleryViewerView.js';
import DOMBuilder from '../lib/domBuilder.js';

(() => {

	// All `.folios figure` elements
	// Folio-Figures can be dispatched in different instances of Gallery Views
	
	const figures = document.querySelectorAll('.folios figure');

    const arcade = new GalleryViewerController( new GalleryViewerView(DOMBuilder, figures) );

})();