/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

class GalleryViewerView {

    constructor(domBuilder, figures) {
        this._dom = domBuilder;
        this._figures = figures;
        // Gallery DOM Elements
        this._gallery = {};
        this._holder = {};
        this._controls = {};
        this._legend = {};
        // Datastore
        this._pictures = [];
        this._ix = 0;
        this._total = figures.length;
    }

    /**
     * Allow folios to be interactive
     */
    enableFolios(){
        for (let i = 0 ; i < this._total ; i++) {
            this._figures[i].setAttribute('data-ix', i);
            this._pictures[i] = {};
            this._pictures[i].img = this._figures[i].querySelector('img').src;
            this._pictures[i].caption = this._figures[i].querySelector('figcaption').innerHTML;
            this._figures[i].addEventListener('click', evt => {
                let val = parseInt(evt.currentTarget.getAttribute('data-ix'));
                this.enableGallery( val < this._total ? val : 0 );
            })
        }
    }

    enableGallery(index){
        // Disable document scrolling
        document.body.style.overflowY = 'hidden';
        // Insert Gallery
        document.body.insertAdjacentElement('beforeend', this._gallery);
        // Update
        this.updateGallery(index);
        // Enable Events
        this.enableEvents();

        console.info('Gallery enabled');
    }

    desableGallery(){
        // Disable Events 
        this.disableEvents();
        // Enable scrolling
        document.body.removeAttribute('style');
        // Remove Gallery
        document.body.removeChild(this._gallery);

        console.info('Gallery disabled');
    }

    buildGallery(){
        // Controls
        this._controls['quit'] = this._dom.nodeCreator('div', this._dom.nodeCreator('button', this._dom.nodeCreator('span', 'Exit', {className:'hide'} ), {type:'button', className:'uinav'}), {id:'quit'});
        
        // Gallery Layer
        this._gallery = this._dom.nodeCreator( 'div', this._controls['quit'], {id:'gallery'} );
        
        // Navigation
        ['prev', 'next'].forEach( key => {
            this._controls[key] = this._dom.nodeCreator( 'div', this._dom.nodeCreator( 'button', this._dom.nodeCreator( 'span', '\u00a0', {className:'hide'} ), {type:'button', className:'uinav'} ), {id:key, className:'seek'} );
            this._gallery.appendChild( this._controls[key] );
        } );
        
        // Image Holder
        this._holder = this._dom.nodeCreator( 'div', '\u00a0', {id:'holder'} );
        this._gallery.appendChild( this._holder );
        
        // Legend
        this._legend = this._dom.nodeCreator( 'div', this._dom.nodeCreator( 'p', '-', null ), {id:'legend'} );
        this._gallery.appendChild( this._legend );
        
        console.info('Gallery ready');
    }

    eventsManager = evt => {
        if (evt.key === 'ArrowLeft' || evt.currentTarget.id === 'prev') this.updateGallery(--this._ix);
        if (evt.key === 'ArrowRight' || evt.currentTarget.id === 'next') this.updateGallery(++this._ix);
        if (evt.key === 'Escape' || evt.currentTarget.id === 'quit') this.desableGallery();
        return;
    }

    enableEvents(){
        this._controls['quit'].addEventListener('click', this.eventsManager);
        this._controls['prev'].addEventListener('click', this.eventsManager);
        this._controls['next'].addEventListener('click', this.eventsManager);
        if (window.KeyboardEvent) window.addEventListener('keydown', this.eventsManager);
    }

    disableEvents(){
        this._controls['quit'].removeEventListener('click', this.eventsManager);
        this._controls['prev'].removeEventListener('click', this.eventsManager);
        this._controls['next'].removeEventListener('click', this.eventsManager);
        if (window.KeyboardEvent) window.removeEventListener('keydown', this.eventsManager);
    }

    updateGallery(seek){
        // Manage index
        this._ix = (seek < 0)? this._total-1 : (this._ix === this._total)? 0 : seek;
        // Set Holder
        this._holder.style = `background-image: url(${this._pictures[this._ix].img})`;
        this._legend.innerHTML = `<p>${this._pictures[this._ix].caption}</p>`;
    }
}

export default GalleryViewerView;