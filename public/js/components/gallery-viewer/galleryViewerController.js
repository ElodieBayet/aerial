/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

class GalleryViewerController {

    constructor(view){
        this._view = view;
        this._aera = 860 * 600;
        this._styles = '/public/css/kit_gallery.min.css';

        this.initialize();
    }

    initialize(){
        if (location.hostname === '127.0.0.1' || location.hostname === 'localhost') this._styles = '/public/css/kit_gallery.css';
        document.head.insertAdjacentHTML('beforeend', '<link href="'+this._styles+'" rel="stylesheet" type="text/css"">');
        if (window.innerHeight * window.innerWidth >= this._aera) {
            this._view.enableFolios();
            this._view.buildGallery();
        }
    }
}

export default GalleryViewerController;