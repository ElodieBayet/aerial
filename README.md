# Aerial :: Personal Front-End Template

Date | Reviewed | Version | Purpose | Live
---- | -------- | ------- | ------- | ----
2020.10 | 2022.04 | 1.1.0 | Demo | [Aerial](https://demo.elodiebayet.com/aerial)


## Summary
0. [Presentation](#markdown-header-0-presentation)
    * [0.0 - Technologies](#markdown-header-00-technologies)
    * [0.1 - Features](#markdown-header-01-features)
    * [0.2 - Usage](#markdown-header-02-usage)
1. [Structure](#markdown-header-1-structure)
    * [1.0 - Folders and files](#markdown-header-10-folders-and-files)
    * [1.1 - HTML Document](#markdown-header-11-html-document)
2. [Development](#markdown-header-2-development)
    * [2.0 - CSS](#markdown-header-20-css)
    * [2.1 - JavaScript](#markdown-header-21-javascript)
3. [Remarks](#markdown-header-3-remarks)

---

## 0 - Presentation

Front-End template for _Resume / Portfolio_ website available on [elodiebayet.com](https://www.elodiebayet.com).


### 0.0 - Technologies

* [NPM](https://www.npmjs.com/)
    * [clean-css-cli](https://www.npmjs.com/package/clean-css-cli) CLI to clean and minify CSS
    * [Npm Run All](https://www.npmjs.com/package/npm-run-all) CLI to run multiple npm-scripts in parallel or sequential


### 0.1 - Features

* [Root](/index.html) Root page for language selection if french and english don't match
* [Base](/base.html) Typographic samples, with titles, paragraphes, lists or any text implementations
* [Components](/components.html) Dedicated blocs, and boutons, links, tables or designed elements
* [Pages](/pages.html) Specific pages and special contents
* [Contact](/contact.html) Page dedicated for contact info and form
* [Modules JS](/modules.html) Live development of JavaScript components and modules
* [Sitemap](/sitemap.html) Content for map of website
* [Error](/error.html) Content for any 4xx errors
* [Unavailable](/unavailable.html) Layout for internal error


### 0.2 - Usage

This project is just a _demo_. It doesn't fit for public or professionnal usage. Reproduction, public communication, partial or entire of this Website or its content are strictly prohibited without an official and written authorization by the Developer.

---

## 1 - Structure

### 1.0 - Folders and files

* `data/` _static files for content, mocked datasets, etc. (json, js, php, txt)_
    `common/`
* `dist/` _minified source code for distribution and project integration_
    - `css/`
    - `js/`
* `public/` _public source code for development_
    - `css/` _styles for layout and structure of components_
        - `base.css`
        - `components.css`
        - `(*).css`
        - `aerial/` _styles for theme and design_
            - `aerial_base.css`
            - `aerial_components.css`
            - `aerial_(*).css`
    - `js/`
        - `components/` _package dedicated to a specific component_
            - `(component-name)/` _MCV folder_
            - `(componentName).js` _main and entry script for component, inserted in the document where component is_
        - `modules/` __cross-application modules for GUI, or else, inserted anywhere_
        - `lib/` _tool classes_
* `store/`
    - `figures/` _global images or pictures for content integration_
    - `fonts/` _local fonts for application_
    - `icons/` _icons, logos, etc. not trademark_
    - `interface/` _backgrounds, pictos, symbols, etc. for user interface_
    - `trademark/` _logos, icons, etc. relative to owner / brand_


### 1.1 - HTML Document

#### Tags, ids and classes

* `<header#uihead>`
    - `.headline`
        - `<h2>` (hidden)
        - `<button>` 
        - `.applogo` 
            - `.icon`
            - `.title` (optional) | `.tip.` (hidden)
        - `<button>`
    - `.navigation` (optional) (max. 2 * menu) 
        - `.mainmenu`
        - `.langmenu` | `.socialmenu` (optional)
* `<main>`
    - `<header>`
        - `<h1>`
        - `.share` (optional)
    - `<section>` | `<article>` | `<div>` {n} blocs or sections dedicated to topics
        - `.grid` | `.columns` | `<div>` {n} sub-structural blocks
* `<footer#uifoot>`
    - `.headline`
        - `.applogo`
            - `<h2>` (hidden)
            - `.icon`
        - `.socialmenu` (optional)
    - `.navigation` (optional)
        - `.secondmenu`

#### Wireframes

![Wireframe Wide #1](./store/figures/wireframe_wide_01.jpg)

#### Mockups

![Mockup Wide #1](./store/figures/mockup_wide_01.jpg)

---

## 2 - Development

### 2.0 - CSS

#### Division

## 2.1 - JavaScript

---

## 3 - Remarks